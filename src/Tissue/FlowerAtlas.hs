module Tissue.FlowerAtlas where

import Tissue.Mesh
import Tissue.Solver
import Tissue.Model
import Tissue.Common
import System.Process
import System.FilePath.Posix

meshesDir = "/home/argyris/home3/genes2shape/ERACAPS/Flower Atlas/Wildtype Time series/yr_01/meshes/"
meshesDir' = "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/"

meshLoc :: Timepoint -> FilePath
meshLoc T10 = meshesDir ++ "t10_auto.ply"
meshLoc T40 = meshesDir ++ "t40_auto.ply"
meshLoc T96 = meshesDir ++ "t96_auto.ply"
meshLoc T120 = meshesDir ++ "t120_auto.ply"
meshLoc T132 = meshesDir ++ "t132_auto.ply"

meshLoc' :: Timepoint -> FilePath
meshLoc' T10 = meshesDir' </> "t10/t10_auto.ply"
meshLoc' T40 = meshesDir' </> "t40/t40_auto.ply" 
meshLoc' T96 = meshesDir' </> "t96/t96_auto.ply" 
meshLoc' T120 = meshesDir' </> "t120/t120_auto.ply" 
meshLoc' T132 = meshesDir' </> "t132/t132_auto.ply" 

data Timepoint = T10 | T40 | T96 | T120 | T132 deriving Show

instance ToInit Timepoint where
  toInit T10 = show 10
  toInit T40 = show 40
  toInit T96 = show 96
  toInit T120 = show 120
  toInit T132 = show 132

next :: Timepoint -> Timepoint
next T10 = T40
next T40 = T96
next T96 = T120
next T120 = T132
next T132 = error ""

mkDefMod :: FilePath -> Timepoint -> IO ()
mkDefMod fout t = do
  let convertExe = "/home/argyris/home3/tissue/bin/converter"
      inputMesh = meshLoc' t
      outInit = fout </> replaceExtension (snd $ splitFileName inputMesh) "init"
      model = AnisoModelUni
      solver = Solver { sst = RK5Adaptive,
                        tt = Range t (next t),
                        mode = Mode 59 5,
                        other = Setts 0.005 1e-4}
      modelF = fout </> "model.model"
      solverF = fout </> "solver.rk5"
      cmd = convertExe ++ " -input_format ply " ++  inputMesh ++ " > " ++ outInit
  callCommand cmd
  tissue <- readGeometry inputMesh
  let tissue' = addLabelToVarT (labelCond (aboveY 200) tissue)
  appendInit tissue' outInit
  writeInit modelF model  
  writeInit solverF solver



