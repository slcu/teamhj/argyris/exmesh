module Tissue.Solver where

import Data.List
import Tissue.Common

data SolverType =
  RK5Adaptive
  deriving (Show)

instance ToInit SolverType where
  toInit = show

data Range t =
  Range t
        t
  deriving (Show)

data Mode =
  Mode Int
       Int
  deriving (Show)

instance ToInit Mode where
  toInit (Mode m npoints) = toInit m ++ " " ++ toInit npoints

instance (ToInit t) => ToInit (Range t) where
  toInit (Range ts te) = toInit ts ++ " " ++ toInit te

data Setts =
  Setts Double
        Double
  deriving (Show)

instance ToInit Setts where
  toInit (Setts s1 s2) = toInit s1 ++ " " ++ toInit s2

data Solver t = Solver
  { sst :: SolverType
  , tt :: Range t
  , mode :: Mode
  , other :: Setts
  } deriving (Show)

instance (ToInit t) => ToInit (Solver t) where
  toInit (Solver {sst = st, tt = r, mode = m, other = setts}) =
    intercalate "\n" [toInit st, toInit r, toInit m, toInit setts]

