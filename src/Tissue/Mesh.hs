{-# LANGUAGE OverloadedStrings #-}

module Tissue.Mesh where

import Tissue.Common
import Control.Lens
import Data.Default
import Linear.Matrix
import Linear.V3
import Linear.V2
import Data.Csv
import Data.Char
import Data.List.Extra
import Data.Vector.Storable (Vector)
import PLY
import PLY.Types
import Control.Monad
import Vis (VisObject, Color)
import qualified Vis as VS
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as B
import qualified Data.Map.Strict as M

data SCell = SCell
  { cid :: Int
  , ctr :: V3 Float
  } deriving (Show)

instance FromRecord SCell where 
 parseRecord v =
    SCell <$> v .! 0 <*> (V3 <$> (v .! 3) <*> (v .! 2) <*> (v .! 1))

data MVec a =
  MVec (V3 a)
       a

instance (ToInit a) => ToInit (V2 a) where
  toInit (V2 x y) = toInit x ++ " "  ++ toInit y
  
instance (ToInit a) => ToInit (V3 a) where
  toInit (V3 x y z) = intercalate " " [toInit x, toInit y, toInit z]
  
instance (ToInit a) => ToInit (MVec a) where
  toInit (MVec v m) = toInit v ++ " " ++ toInit m

data CellState = CellState
  { mt :: MVec Float
  , stress :: MVec Float
  , strain :: MVec Float
  , e :: V3 Float
  , ccz :: Float
  , lyoung :: Float
  , astrain :: Float
  , astress :: Float
  , areaRat :: Float
  , isoE :: Float
  , mtStress :: Float
  , vel :: Float
  , mtStressAng :: Float
  , stressStrainAng :: Float
  , vol :: Float
  , stressT :: M23 Float
  , normal :: V3 Float
  }
---to check unisoE as well??

instance ToInit CellState where
  toInit CellState { mt = mt
                   , stress = stress
                   , strain = strain
                   , e = e
                   , ccz = ccz
                   , lyoung = lyoung
                   , astrain = astrain
                   , astress = astress
                   , areaRat = areaRat
                   , isoE = isoE
                   , mtStress = mtStress
                   , vel = vel
                   , mtStressAng = mtStressAng
                   , stressStrainAng = stressStrainAng
                   , vol = vol
                   , stressT = stressT
                   , normal = normal
                   } =
      intercalate " " $
       [ toInit mt
       , toInit stress
       , toInit strain
       , toInit e
       , toInit ccz
       , toInit lyoung
       , toInit astrain
       , toInit astress
       , toInit areaRat
       , toInit isoE
       , toInit mtStress
       , toInit vel
       , toInit mtStressAng
       , toInit stressStrainAng
       , toInit vol
       , toInit stressT
       , toInit normal
       ]

instance Default CellState where
  def =
    CellState
      { mt = MVec (V3 1 0 0) 1
      , stress = MVec (V3 1 0 0) 1
      , strain = MVec (V3 1 1 0) 1
      , e = V3 1 0 0
      , ccz = 0
      , lyoung = 0
      , astrain = 0
      , astress = 0
      , areaRat = 0
      , isoE = 0
      , mtStress = 0
      , vel = 1
      , mtStressAng = 0
      , stressStrainAng = 0
      , vol = 0
      , stressT = V2 (V3 0 0 0) (V3 0 0 0)
      , normal = V3 0 0 0
      }

data Label
  = NoLabel
  | Label Int
  deriving (Show)

data MElement a =
  Triangle (V3 a)
           (V3 a)
           (V3 a)
  deriving (Show)

data Cell s = Cell
  { cel :: MElement Float
  , label :: Label
  , st :: s
  }

data Tissue s = Tissue
  { cells :: [Cell s]
  , verts :: M.Map (V3 Float) Int
  }

instance (ToInit s) => ToInit (Cell s) where
  toInit (Cell{cel=_, label=_, st=s}) = toInit s

instance (ToInit s) => ToInit (Tissue s) where
  toInit (Tissue{cells=ts, verts=_}) = intercalate "\n" (map toInit ts)

----IO
toPLY :: Tissue s -> String
toPLY (Tissue {cells = cs, verts = vs}) = unlines (map (printCell vs) cs)
  where
    printCell vs (Cell {cel = Triangle p1 p2 p3, label = Label lb, st = _}) =
      intercalate " " (map show [3, vs M.! p1, vs M.! p2, vs M.! p3, lb])

readCellCtr :: FilePath -> IO [SCell]
readCellCtr fin = do
  csvData <- BL.readFile fin
  case decodeWith
         (defaultDecodeOptions {decDelimiter = fromIntegral $ ord ' '})
         NoHeader
         csvData of
    Left err -> error err
    Right v -> return $ V.toList v

readGeometry :: FilePath -> IO (Tissue CellState)
readGeometry fts = do
  fts <- readPLY fts
  let tissue = mkTissue fts
  return tissue

readPLY :: FilePath -> IO PLYData
readPLY f = do
  contents <- B.readFile f
  case (preloadPly contents) of
    Left err -> error err
    Right plyd -> return plyd

plyIntToHs :: Scalar -> Int
plyIntToHs (Sint i) = i
plyIntToHs _ = error "expected int"

getVertices :: PLYData -> VS.Vector (V3 Float)
getVertices plyd =
  case (loadPlyElementsV3 "vertex" plyd) of
    Left err -> error err
    Right v -> VS.map zyxToXyz v

mkVertIdx :: VS.Vector (V3 Float) -> M.Map (V3 Float) Int
mkVertIdx vs = M.fromList (zip (VS.toList vs) [0 .. (VS.length vs - 1)])

getFaces :: PLYData -> V.Vector (V.Vector Int)
getFaces plyd =
  case (loadPlyElements "face" plyd) of
    Left err -> error err
    Right v -> V.map (V.map plyIntToHs) v

mkCell :: VS.Vector (V3 Float) -> V.Vector Int -> Cell CellState
mkCell vs is = Cell (Triangle p1 p2 p3) NoLabel def
  where
    p1 = vs VS.! (is V.! 0)
    p2 = vs VS.! (is V.! 1)
    p3 = vs VS.! (is V.! 2)

mkTissue :: PLYData -> Tissue CellState
mkTissue plyd =
  Tissue
    {cells = V.toList (V.map (mkCell verts) faces), verts = mkVertIdx verts}
  where
    verts = getVertices plyd
    faces = getFaces plyd


-----------------

dist :: V3 Float -> V3 Float -> Float
dist a b = sqrt ds
  where
    ds =
      (a ^. _x - b ^. _x) ** 2 + (a ^. _y - b ^. _y) ** 2 +
      (a ^. _z - b ^. _z) ** 2

zyxToXyz :: V3 Float -> V3 Float
zyxToXyz (V3 z y x) = V3 x y z

xyzToZyx :: V3 Float -> V3 Float
xyzToZyx (V3 x y z) = V3 z y x

v3Sum :: (Num a) => V3 a -> V3 a -> V3 a
v3Sum (V3 x y z) (V3 x' y' z') = V3 (x + x') (y + y') (z + z')

dn :: (Fractional a) => V3 a -> a -> V3 a
dn (V3 x y z) n = V3 (x / n) (y / n) (z / n)

centre :: (Fractional a) => MElement a -> V3 a
centre (Triangle m1 m2 m3) = dn (v3Sum (v3Sum m1 m2) m3) 3

---------------
band :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
band = liftM2 (&&)

bor :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
bor = liftM2 (||)

bnot :: (a-> Bool) -> (a -> Bool)
bnot = liftM not

aboveY :: Float -> Cell s -> Bool
aboveY yv (Cell {cel = Triangle p1 p2 p3, label = _, st = _}) =
  (p1 ^. _y > yv) && (p2 ^. _y > yv) && (p3 ^. _y > yv)

betweenZ :: Float -> Float -> Cell s -> Bool
betweenZ z1 z2  (Cell {cel = Triangle p1 p2 p3, label = _, st = _}) =
  (p1 ^. _z > z1 && p1 ^. _z < z2) &&
  (p2 ^. _z > z1 && p2 ^. _z < z2) &&
  (p3 ^. _z > z1 && p3 ^. _z < z2)

betweenX :: Float -> Float -> Cell s -> Bool
betweenX x1 x2  (Cell {cel = Triangle p1 p2 p3, label = _, st = _}) =
  (p1 ^. _x > x1 && p1 ^. _x < x2) &&
  (p2 ^. _x > x1 && p2 ^. _x < x2) &&
  (p3 ^. _x > x1 && p3 ^. _x < x2) 

assignL :: [SCell] -> Cell s -> Cell s
assignL scs (Cell el _ s) = Cell el (Label lb) s
  where
    tc = centre el
    lds = [(cid sc, dist (ctr sc) tc) | sc <- scs]
    (lb, _) = minimumOn (\(i, d) -> d) lds

assignLabels :: [SCell] -> Tissue s -> Tissue s
assignLabels cs Tissue {cells = ts, verts = vs} =
  Tissue {cells = map (assignL cs) ts, verts = vs}

labelTs :: FilePath -> FilePath -> IO (Tissue CellState)
labelTs fcs fts = do
  cs <- readCellCtr fcs
  fts <- readPLY fts
  let tissue = mkTissue fts
  return $ assignLabels cs tissue

addLabelToVarT :: Tissue CellState -> Tissue CellState
addLabelToVarT Tissue {cells = ts, verts = vs} =
  Tissue {cells = map addLabelToVar ts, verts = vs}
  where
    addLabelToVar Cell {cel = cel, label = Label 1, st = _} =
      Cell {cel = cel, label = Label 1, st = def {e = V3 1 1.0 0}}
    addLabelToVar Cell {cel = cel, label = Label 2, st = _} =
      Cell {cel = cel, label = Label 2, st = def {e = V3 1 0.0 0}}

labelCond :: (Cell CellState -> Bool) -> Tissue CellState -> Tissue CellState
labelCond fb (Tissue {cells=ts, verts=vs}) = Tissue {cells=cells', verts=vs}
  where
    cells' = [c {label=if fb c then (Label 1) else (Label 2)} | c <- ts]


--- some other utility functions
lbId :: Cell s -> Int
lbId (Cell{cel=_, label=Label i, st=_}) = i
lbId (Cell{cel=_, label=NoLabel, st=_}) = error ""

labelFreqs :: Tissue s -> [(Int, Int)]
labelFreqs (Tissue {cells=ts, verts=_}) = freqs lbs
  where
    lbs = map lbId ts

freqs xs = map (\x -> (head x, length x)) . group . sort $ xs



---- testing
goInit = do
   tissue <- readGeometry "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t40/t40.ply"
   let tissue' = addLabelToVarT (labelCond (bnot (betweenX 80 250)) tissue)
   appendInit tissue' "/home/argyris/home3/genes2shape/sims1/r8/t40.init"
