module Tissue.Common where

class ToInit a where
  toInit :: a -> String

instance ToInit Double where
  toInit = show

instance ToInit Int where
  toInit = show

instance ToInit Float where
  toInit f = show f

writeInit :: (ToInit a) => FilePath -> a -> IO ()
writeInit fout v = writeFile fout (toInit v)

appendInit ts f = appendFile f (toInit ts)
